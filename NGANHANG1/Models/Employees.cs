﻿using System.ComponentModel.DataAnnotations;

namespace NganHang.Models
{
    public class Employees
    {
        [Key]
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string AddTextHere { get; set; }
        public ICollection<Transactions> transactions { get; set; }
    }
}
