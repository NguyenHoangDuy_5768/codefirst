﻿using System.ComponentModel.DataAnnotations;

namespace NganHang.Models
{
    public class Accounts
    {
        [Key]
        public int AccountsID { get; set; }
        public int CustomerID { get; set; }
        public string AccountName { get; set; }
        public string AddTextHere { get; set; }
        public Customer? customer { get; set; }
        public ICollection<Reports> reports { get; set; }
    }
}
