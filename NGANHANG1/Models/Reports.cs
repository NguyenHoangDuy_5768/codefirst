﻿using System.ComponentModel.DataAnnotations;

namespace NganHang.Models
{
    public class Reports
    {
        [Key]
        public int ReportID {  get; set; }
        public int AccountID {  get; set; }
        public int LogsID { get; set; }
        public int TransactionalID {  get; set; }
        public string Reportname {  get; set; }
        public DateTime? Reportdate { get; set; }
        public string AddTextHere { get; set;}
        public Accounts? Accounts { get; set; }
        public Logs? Logs { get; set; }
        public Transactions? Transactions { get; set; }

    }
}
