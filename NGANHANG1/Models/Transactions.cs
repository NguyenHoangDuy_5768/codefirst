﻿using System.ComponentModel.DataAnnotations;

namespace NganHang.Models
{
    public class Transactions
    {
        [Key] 
        public int TransactionID { get; set; }
        public int EmployeeID { get; set; }
        public int CustomerID {  get; set; }
        public string Name { get; set; }
        public string AddTextHere { get; set; }
        public Employees? Employees { get; set; }
        public Customer? Customer { get; set; }
        public ICollection<Reports> Reports { get; set; }
        public ICollection<Logs> Logs { get; set; }

    }
}
