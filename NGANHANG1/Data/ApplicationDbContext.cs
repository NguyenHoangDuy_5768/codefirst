﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NganHang.Models;

namespace NGANHANG1.Data
{
    public class ApplicationDbContext : DbContext 
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Reports> Reports { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
    }
}
