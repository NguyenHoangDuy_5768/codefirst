﻿using System.ComponentModel.DataAnnotations;

namespace NganHang.Models
{
    public class Customer
    {
        [Key]
        public int CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string Username { get; set; }
        public string Passwork { get; set; }
        public string AddTextHere { get; set; }
        public ICollection<Accounts> accounts {  get; set; }
        public ICollection<Transactions> transactions { get; set; }
    }
}
