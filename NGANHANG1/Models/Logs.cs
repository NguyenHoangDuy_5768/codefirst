﻿using System.ComponentModel.DataAnnotations;

namespace NganHang.Models
{
    public class Logs
    {
        [Key]
        public int LogsID { get; set; }
        public int TransactionID { get; set; }
        public DateTime? Logindate { get; set; }
        public DateTime? Logintime { get; set; }
        public string AddTextHere { get; set; }
        public Transactions? Transactions { get; set; }
        public ICollection<Reports> Reports { get; set; }
    }
}
